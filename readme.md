# CL Companion

This is the project I worked on for the 3 days I had in development as a SEIR.
It's an app to help Cohort Leads see which students have been doing the Practice Problems every day. 
I didn't get it finished, but it's fairly functional for what's there.
I built it using Selenium, BeautifulSoup, and Tkinter.
The original idea was to get it built and package it with PyInstaller so that it could be easily used by Cohort Leads as a stand-alone application, but didn't get far enough to try that out. I think Selenium would package well with PyInstaller. My only concern would be with it getting the right chromedriver. The one included with the Repo is for Chrome 117. Also it would need to be bundled once on MacOS and again on Windows since PyInstaller bundles it for the Operating System it's being developed on.

If anyone for whatever reason decides they want to pick it up where I left off, I left comments on what everything does. If you have any questions, feel free to shoot me an email: john.gardner823@gmail.com